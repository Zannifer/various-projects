﻿namespace Players_Handbook
{
    partial class SpellBookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxSpellNames = new System.Windows.Forms.ListBox();
            this.rtbDisplaySpell = new System.Windows.Forms.RichTextBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbRitual = new System.Windows.Forms.CheckBox();
            this.tbDuration = new System.Windows.Forms.TextBox();
            this.tbComponents = new System.Windows.Forms.TextBox();
            this.tbRange = new System.Windows.Forms.TextBox();
            this.tbCastTime = new System.Windows.Forms.TextBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbxLevel = new System.Windows.Forms.ComboBox();
            this.cbxSchool = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbWizard = new System.Windows.Forms.CheckBox();
            this.cbWarlock = new System.Windows.Forms.CheckBox();
            this.cbSorcerer = new System.Windows.Forms.CheckBox();
            this.cbRanger = new System.Windows.Forms.CheckBox();
            this.cbPaladin = new System.Windows.Forms.CheckBox();
            this.cbDruid = new System.Windows.Forms.CheckBox();
            this.cbCleric = new System.Windows.Forms.CheckBox();
            this.cbBard = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbxSpellNames
            // 
            this.lbxSpellNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxSpellNames.FormattingEnabled = true;
            this.lbxSpellNames.ItemHeight = 16;
            this.lbxSpellNames.Location = new System.Drawing.Point(12, 12);
            this.lbxSpellNames.Name = "lbxSpellNames";
            this.lbxSpellNames.Size = new System.Drawing.Size(225, 420);
            this.lbxSpellNames.TabIndex = 0;
            this.lbxSpellNames.SelectedIndexChanged += new System.EventHandler(this.lbxSpellNames_SelectedIndexChanged);
            // 
            // rtbDisplaySpell
            // 
            this.rtbDisplaySpell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDisplaySpell.Location = new System.Drawing.Point(243, 12);
            this.rtbDisplaySpell.Name = "rtbDisplaySpell";
            this.rtbDisplaySpell.Size = new System.Drawing.Size(314, 420);
            this.rtbDisplaySpell.TabIndex = 1;
            this.rtbDisplaySpell.Text = "";
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(12, 437);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(108, 35);
            this.btnRemove.TabIndex = 2;
            this.btnRemove.Text = "REMOVE SPELL";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(8, 360);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(102, 53);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "ADD SPELL";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbRitual);
            this.groupBox1.Controls.Add(this.tbDuration);
            this.groupBox1.Controls.Add(this.tbComponents);
            this.groupBox1.Controls.Add(this.tbRange);
            this.groupBox1.Controls.Add(this.tbCastTime);
            this.groupBox1.Controls.Add(this.rtbDescription);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbxLevel);
            this.groupBox1.Controls.Add(this.cbxSchool);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Location = new System.Drawing.Point(563, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 419);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Spell Entry";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 233);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "DESCRIPTION:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(54, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "RANGE: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "COMPONENTS:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 205);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "DURATION:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "CASTING TIME:";
            // 
            // cbRitual
            // 
            this.cbRitual.AutoSize = true;
            this.cbRitual.Location = new System.Drawing.Point(111, 100);
            this.cbRitual.Name = "cbRitual";
            this.cbRitual.Size = new System.Drawing.Size(98, 17);
            this.cbRitual.TabIndex = 11;
            this.cbRitual.Text = "Is this a Ritual?";
            this.cbRitual.UseVisualStyleBackColor = true;
            // 
            // tbDuration
            // 
            this.tbDuration.Location = new System.Drawing.Point(111, 202);
            this.tbDuration.Name = "tbDuration";
            this.tbDuration.Size = new System.Drawing.Size(170, 20);
            this.tbDuration.TabIndex = 10;
            // 
            // tbComponents
            // 
            this.tbComponents.Location = new System.Drawing.Point(111, 176);
            this.tbComponents.Name = "tbComponents";
            this.tbComponents.Size = new System.Drawing.Size(170, 20);
            this.tbComponents.TabIndex = 9;
            // 
            // tbRange
            // 
            this.tbRange.Location = new System.Drawing.Point(111, 150);
            this.tbRange.Name = "tbRange";
            this.tbRange.Size = new System.Drawing.Size(170, 20);
            this.tbRange.TabIndex = 8;
            // 
            // tbCastTime
            // 
            this.tbCastTime.Location = new System.Drawing.Point(111, 123);
            this.tbCastTime.Name = "tbCastTime";
            this.tbCastTime.Size = new System.Drawing.Size(170, 20);
            this.tbCastTime.TabIndex = 7;
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(6, 249);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(275, 164);
            this.rtbDescription.TabIndex = 6;
            this.rtbDescription.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "LEVEL:";
            // 
            // cbxLevel
            // 
            this.cbxLevel.FormattingEnabled = true;
            this.cbxLevel.Location = new System.Drawing.Point(111, 72);
            this.cbxLevel.Name = "cbxLevel";
            this.cbxLevel.Size = new System.Drawing.Size(88, 21);
            this.cbxLevel.TabIndex = 4;
            // 
            // cbxSchool
            // 
            this.cbxSchool.FormattingEnabled = true;
            this.cbxSchool.Location = new System.Drawing.Point(111, 45);
            this.cbxSchool.Name = "cbxSchool";
            this.cbxSchool.Size = new System.Drawing.Size(170, 21);
            this.cbxSchool.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "SPELL SCHOOL:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "SPELL NAME:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(111, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(170, 20);
            this.tbName.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbWizard);
            this.groupBox2.Controls.Add(this.cbWarlock);
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.cbSorcerer);
            this.groupBox2.Controls.Add(this.cbRanger);
            this.groupBox2.Controls.Add(this.cbPaladin);
            this.groupBox2.Controls.Add(this.cbDruid);
            this.groupBox2.Controls.Add(this.cbCleric);
            this.groupBox2.Controls.Add(this.cbBard);
            this.groupBox2.Location = new System.Drawing.Point(856, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(116, 419);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Class Options";
            // 
            // cbWizard
            // 
            this.cbWizard.AutoSize = true;
            this.cbWizard.Location = new System.Drawing.Point(7, 198);
            this.cbWizard.Name = "cbWizard";
            this.cbWizard.Size = new System.Drawing.Size(70, 17);
            this.cbWizard.TabIndex = 7;
            this.cbWizard.Text = "WIZARD";
            this.cbWizard.UseVisualStyleBackColor = true;
            // 
            // cbWarlock
            // 
            this.cbWarlock.AutoSize = true;
            this.cbWarlock.Location = new System.Drawing.Point(7, 174);
            this.cbWarlock.Name = "cbWarlock";
            this.cbWarlock.Size = new System.Drawing.Size(80, 17);
            this.cbWarlock.TabIndex = 6;
            this.cbWarlock.Text = "WARLOCK";
            this.cbWarlock.UseVisualStyleBackColor = true;
            // 
            // cbSorcerer
            // 
            this.cbSorcerer.AutoSize = true;
            this.cbSorcerer.Location = new System.Drawing.Point(7, 150);
            this.cbSorcerer.Name = "cbSorcerer";
            this.cbSorcerer.Size = new System.Drawing.Size(86, 17);
            this.cbSorcerer.TabIndex = 5;
            this.cbSorcerer.Text = "SORCERER";
            this.cbSorcerer.UseVisualStyleBackColor = true;
            // 
            // cbRanger
            // 
            this.cbRanger.AutoSize = true;
            this.cbRanger.Location = new System.Drawing.Point(7, 126);
            this.cbRanger.Name = "cbRanger";
            this.cbRanger.Size = new System.Drawing.Size(72, 17);
            this.cbRanger.TabIndex = 4;
            this.cbRanger.Text = "RANGER";
            this.cbRanger.UseVisualStyleBackColor = true;
            // 
            // cbPaladin
            // 
            this.cbPaladin.AutoSize = true;
            this.cbPaladin.Location = new System.Drawing.Point(7, 102);
            this.cbPaladin.Name = "cbPaladin";
            this.cbPaladin.Size = new System.Drawing.Size(72, 17);
            this.cbPaladin.TabIndex = 3;
            this.cbPaladin.Text = "PALADIN";
            this.cbPaladin.UseVisualStyleBackColor = true;
            // 
            // cbDruid
            // 
            this.cbDruid.AutoSize = true;
            this.cbDruid.Location = new System.Drawing.Point(7, 78);
            this.cbDruid.Name = "cbDruid";
            this.cbDruid.Size = new System.Drawing.Size(61, 17);
            this.cbDruid.TabIndex = 2;
            this.cbDruid.Text = "DRUID";
            this.cbDruid.UseVisualStyleBackColor = true;
            // 
            // cbCleric
            // 
            this.cbCleric.AutoSize = true;
            this.cbCleric.Location = new System.Drawing.Point(7, 54);
            this.cbCleric.Name = "cbCleric";
            this.cbCleric.Size = new System.Drawing.Size(64, 17);
            this.cbCleric.TabIndex = 1;
            this.cbCleric.Text = "CLERIC";
            this.cbCleric.UseVisualStyleBackColor = true;
            // 
            // cbBard
            // 
            this.cbBard.AutoSize = true;
            this.cbBard.Location = new System.Drawing.Point(7, 30);
            this.cbBard.Name = "cbBard";
            this.cbBard.Size = new System.Drawing.Size(56, 17);
            this.cbBard.TabIndex = 0;
            this.cbBard.Text = "BARD";
            this.cbBard.UseVisualStyleBackColor = true;
            // 
            // SpellBookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 484);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.rtbDisplaySpell);
            this.Controls.Add(this.lbxSpellNames);
            this.Name = "SpellBookForm";
            this.Text = "Spellbook";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbxSpellNames;
        private System.Windows.Forms.RichTextBox rtbDisplaySpell;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbSorcerer;
        private System.Windows.Forms.CheckBox cbRanger;
        private System.Windows.Forms.CheckBox cbPaladin;
        private System.Windows.Forms.CheckBox cbDruid;
        private System.Windows.Forms.CheckBox cbCleric;
        private System.Windows.Forms.CheckBox cbBard;
        private System.Windows.Forms.CheckBox cbWizard;
        private System.Windows.Forms.CheckBox cbWarlock;
        private System.Windows.Forms.CheckBox cbRitual;
        private System.Windows.Forms.TextBox tbDuration;
        private System.Windows.Forms.TextBox tbComponents;
        private System.Windows.Forms.TextBox tbRange;
        private System.Windows.Forms.TextBox tbCastTime;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbxLevel;
        private System.Windows.Forms.ComboBox cbxSchool;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
    }
}