﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Players_Handbook
{
    public partial class SpellBookForm : Form
    {
        public SpellBookForm()
        {
            InitializeComponent();
            loadSpellNames();
            loadSpellLevels();
            loadSchoolNames();
            
        }
        private void loadSchoolNames()
        {
            cbxSchool.Items.Add("Abjuration");
            cbxSchool.Items.Add("Conjuration");
            cbxSchool.Items.Add("Divination");
            cbxSchool.Items.Add("Enchantment");
            cbxSchool.Items.Add("Evocation");
            cbxSchool.Items.Add("Illusion");
            cbxSchool.Items.Add("Necromancy");
            cbxSchool.Items.Add("Transmutation");            
        }
        private void loadSpellLevels()
        {
            cbxLevel.Items.Add("0");
            cbxLevel.Items.Add("1");
            cbxLevel.Items.Add("2");
            cbxLevel.Items.Add("3");
            cbxLevel.Items.Add("4");
            cbxLevel.Items.Add("5");
            cbxLevel.Items.Add("6");
            cbxLevel.Items.Add("7");
            cbxLevel.Items.Add("8");
            cbxLevel.Items.Add("9");            
        }
        private void loadSpellNames()
        {
            lbxSpellNames.Items.Clear();
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                string stm = "Select spell_name from handbook.spellbook order by spell_name";
                MySqlCommand cmd = new MySqlCommand(stm, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    lbxSpellNames.Items.Add(rdr.GetString(0));
                }
            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
           string connetionString = null;
            MySqlConnection cnn;
            connetionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            cnn = new MySqlConnection(connetionString);
            cnn.Open();
            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM handbook.spellbook WHERE spell_name = '" + lbxSpellNames.SelectedItem.ToString() + "'", cnn))
            {
                try
                {
                    MySqlDataReader rdr = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    string s = String.Format("Error: {0}", ex.ToString());
                    MessageBox.Show(s);
                }
                finally
                {
                    loadSpellNames();
                    rtbDisplaySpell.Clear();
                } 
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string classString = "";
            if (cbBard.Checked)
            {
                classString += "Bard ";
            }
            if (cbCleric.Checked)
            {
                classString += "Cleric ";
            }
            if (cbDruid.Checked)
            {
                classString += "Druid ";       
            }
            if (cbPaladin.Checked)
            {
                classString += "Paladin ";             
            }
            if (cbRanger.Checked)
            {
                classString += "Ranger ";
            }
            if (cbSorcerer.Checked)
            {
                classString += "Sorcerer ";                
            }
            if (cbWarlock.Checked)
            {
                classString += "Warlock ";
            }
            if (cbWizard.Checked)
            {
                classString += "Wizard ";
            }                    
            
            try
            {
                string connetionString = null;
                MySqlConnection cnn;
                connetionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
                cnn = new MySqlConnection(connetionString);
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = "INSERT INTO handbook.spellbook(spell_name, spell_level, ritual, school, cast_time, components, duration, spell_range, description, class) VALUES(@spell_name, @spell_level, @ritual, @school, @cast_time, @components, @duration, @spell_range, @description, @class)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@spell_name", tbName.Text);
                cmd.Parameters.AddWithValue("@spell_level", Convert.ToInt32(cbxLevel.SelectedItem));
                cmd.Parameters.AddWithValue("@ritual", cbRitual.Checked);
                cmd.Parameters.AddWithValue("@school", cbxSchool.SelectedItem);
                cmd.Parameters.AddWithValue("@cast_time", tbCastTime.Text);
                cmd.Parameters.AddWithValue("@components", tbComponents.Text);
                cmd.Parameters.AddWithValue("@duration", tbDuration.Text);
                cmd.Parameters.AddWithValue("@spell_range", tbRange.Text);
                cmd.Parameters.AddWithValue("@description", rtbDescription.Text);
                cmd.Parameters.AddWithValue("@class", classString);

                cmd.ExecuteNonQuery();

                cnn.Close();
                loadSpellNames();
            }
            catch (Exception ex)
            {
                string s = String.Format("Error: {0}", ex.ToString());
                MessageBox.Show(s);
            }
        }

        private void lbxSpellNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            rtbDisplaySpell.Clear();
            string result = "";
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                string stm = "Select * from handbook.spellbook where spell_name = '" + lbxSpellNames.SelectedItem + "'";
                MySqlCommand cmd = new MySqlCommand(stm, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if(rdr.GetInt32(1) == 0)
                    {
                        result += rdr.GetString(0) + "\n" + rdr.GetString(3) + " cantrip";
                    }
                    else if (rdr.GetInt32(1) == 1)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3);
                        }
                    }
                    else if(rdr.GetInt32(1) == 2)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3);
                        }
                    }
                    else if (rdr.GetInt32(1) == 3)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3);
                        }
                    }
                    else
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3);
                        }
                    }
                    result += "\nCasting Time: " + rdr.GetString(4);
                    result += "\nRange: " + rdr.GetString(7);
                    result += "\nComponents: " + rdr.GetString(5);
                    result += "\nDuration: " + rdr.GetString(6);
                    result += "\n\n" + rdr.GetString(8);
                }
                rtbDisplaySpell.Text = result;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }




    }
}
