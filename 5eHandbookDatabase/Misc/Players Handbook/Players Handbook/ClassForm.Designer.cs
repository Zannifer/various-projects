﻿namespace Players_Handbook
{
    partial class ClassForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbClasses = new System.Windows.Forms.ListBox();
            this.tbClassName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbHitDice = new System.Windows.Forms.TextBox();
            this.tbHitPoints1 = new System.Windows.Forms.TextBox();
            this.tbHitPoints2 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbSpecial = new System.Windows.Forms.RichTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbEquip = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbSkills = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbSaves = new System.Windows.Forms.TextBox();
            this.tbTools = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbWeapons = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbArmor = new System.Windows.Forms.TextBox();
            this.rtbFeatures = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnNewFeatures = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tbSpecialName = new System.Windows.Forms.TextBox();
            this.cbClassName = new System.Windows.Forms.ComboBox();
            this.btnCreateClass = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbClasses
            // 
            this.lbClasses.FormattingEnabled = true;
            this.lbClasses.Location = new System.Drawing.Point(13, 13);
            this.lbClasses.Name = "lbClasses";
            this.lbClasses.Size = new System.Drawing.Size(120, 225);
            this.lbClasses.TabIndex = 0;
            // 
            // tbClassName
            // 
            this.tbClassName.Location = new System.Drawing.Point(90, 19);
            this.tbClassName.Name = "tbClassName";
            this.tbClassName.Size = new System.Drawing.Size(182, 20);
            this.tbClassName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "CLASS NAME:";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(9, 67);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(263, 179);
            this.rtbDescription.TabIndex = 5;
            this.rtbDescription.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "DESCRIPTION: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "HIT DICE:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 307);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "HIT POINTS AT 1st LEVEL:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 346);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "HIT POINTS AT HIGHER LEVELS:";
            // 
            // tbHitDice
            // 
            this.tbHitDice.Location = new System.Drawing.Point(6, 277);
            this.tbHitDice.Name = "tbHitDice";
            this.tbHitDice.Size = new System.Drawing.Size(263, 20);
            this.tbHitDice.TabIndex = 10;
            // 
            // tbHitPoints1
            // 
            this.tbHitPoints1.Location = new System.Drawing.Point(6, 320);
            this.tbHitPoints1.Name = "tbHitPoints1";
            this.tbHitPoints1.Size = new System.Drawing.Size(263, 20);
            this.tbHitPoints1.TabIndex = 11;
            // 
            // tbHitPoints2
            // 
            this.tbHitPoints2.Location = new System.Drawing.Point(6, 362);
            this.tbHitPoints2.Name = "tbHitPoints2";
            this.tbHitPoints2.Size = new System.Drawing.Size(263, 20);
            this.tbHitPoints2.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtbSpecial);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.tbEquip);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tbSkills);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.tbSaves);
            this.groupBox1.Controls.Add(this.tbTools);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.tbWeapons);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbArmor);
            this.groupBox1.Controls.Add(this.rtbFeatures);
            this.groupBox1.Controls.Add(this.tbClassName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbHitPoints2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbHitPoints1);
            this.groupBox1.Controls.Add(this.rtbDescription);
            this.groupBox1.Controls.Add(this.tbHitDice);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(139, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 658);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New Class";
            // 
            // rtbSpecial
            // 
            this.rtbSpecial.Location = new System.Drawing.Point(6, 445);
            this.rtbSpecial.Name = "rtbSpecial";
            this.rtbSpecial.Size = new System.Drawing.Size(263, 199);
            this.rtbSpecial.TabIndex = 28;
            this.rtbSpecial.Text = "";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 429);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(173, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "SPECIALIZATION DESCRIPTION:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(290, 185);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "FEATURES:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(318, 154);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "EQUIPMENT:";
            // 
            // tbEquip
            // 
            this.tbEquip.Location = new System.Drawing.Point(398, 151);
            this.tbEquip.Name = "tbEquip";
            this.tbEquip.Size = new System.Drawing.Size(267, 20);
            this.tbEquip.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(346, 127);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "SKILLS:";
            // 
            // tbSkills
            // 
            this.tbSkills.Location = new System.Drawing.Point(398, 124);
            this.tbSkills.Name = "tbSkills";
            this.tbSkills.Size = new System.Drawing.Size(267, 20);
            this.tbSkills.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(290, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "SAVING THROWS:";
            // 
            // tbSaves
            // 
            this.tbSaves.Location = new System.Drawing.Point(398, 97);
            this.tbSaves.Name = "tbSaves";
            this.tbSaves.Size = new System.Drawing.Size(267, 20);
            this.tbSaves.TabIndex = 20;
            // 
            // tbTools
            // 
            this.tbTools.Location = new System.Drawing.Point(398, 71);
            this.tbTools.Name = "tbTools";
            this.tbTools.Size = new System.Drawing.Size(267, 20);
            this.tbTools.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(346, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "TOOLS:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(327, 48);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "WEAPONS:";
            // 
            // tbWeapons
            // 
            this.tbWeapons.Location = new System.Drawing.Point(398, 45);
            this.tbWeapons.Name = "tbWeapons";
            this.tbWeapons.Size = new System.Drawing.Size(267, 20);
            this.tbWeapons.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(342, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "ARMOR:";
            // 
            // tbArmor
            // 
            this.tbArmor.Location = new System.Drawing.Point(398, 19);
            this.tbArmor.Name = "tbArmor";
            this.tbArmor.Size = new System.Drawing.Size(267, 20);
            this.tbArmor.TabIndex = 14;
            // 
            // rtbFeatures
            // 
            this.rtbFeatures.Location = new System.Drawing.Point(293, 201);
            this.rtbFeatures.Name = "rtbFeatures";
            this.rtbFeatures.Size = new System.Drawing.Size(372, 443);
            this.rtbFeatures.TabIndex = 13;
            this.rtbFeatures.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnNewFeatures);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.tbSpecialName);
            this.groupBox2.Controls.Add(this.cbClassName);
            this.groupBox2.Location = new System.Drawing.Point(827, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 465);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Specializations";
            // 
            // btnNewFeatures
            // 
            this.btnNewFeatures.Location = new System.Drawing.Point(9, 93);
            this.btnNewFeatures.Name = "btnNewFeatures";
            this.btnNewFeatures.Size = new System.Drawing.Size(20, 20);
            this.btnNewFeatures.TabIndex = 3;
            this.btnNewFeatures.Text = "+";
            this.btnNewFeatures.UseVisualStyleBackColor = true;
            this.btnNewFeatures.Click += new System.EventHandler(this.btnNewFeatures_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 52);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(131, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "SPECIALIZATION NAME:";
            // 
            // tbSpecialName
            // 
            this.tbSpecialName.Location = new System.Drawing.Point(6, 67);
            this.tbSpecialName.Name = "tbSpecialName";
            this.tbSpecialName.Size = new System.Drawing.Size(278, 20);
            this.tbSpecialName.TabIndex = 1;
            // 
            // cbClassName
            // 
            this.cbClassName.FormattingEnabled = true;
            this.cbClassName.Location = new System.Drawing.Point(6, 22);
            this.cbClassName.Name = "cbClassName";
            this.cbClassName.Size = new System.Drawing.Size(278, 21);
            this.cbClassName.TabIndex = 0;
            // 
            // btnCreateClass
            // 
            this.btnCreateClass.Location = new System.Drawing.Point(12, 344);
            this.btnCreateClass.Name = "btnCreateClass";
            this.btnCreateClass.Size = new System.Drawing.Size(120, 43);
            this.btnCreateClass.TabIndex = 16;
            this.btnCreateClass.Text = "CREATE NEW CLASS";
            this.btnCreateClass.UseVisualStyleBackColor = true;
            this.btnCreateClass.Click += new System.EventHandler(this.btnCreateClass_Click);
            // 
            // ClassForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1129, 683);
            this.Controls.Add(this.btnCreateClass);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbClasses);
            this.Name = "ClassForm";
            this.Text = "CLASSES";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbClasses;
        private System.Windows.Forms.TextBox tbClassName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbHitDice;
        private System.Windows.Forms.TextBox tbHitPoints1;
        private System.Windows.Forms.TextBox tbHitPoints2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox rtbFeatures;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tbEquip;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbSkills;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbSaves;
        private System.Windows.Forms.TextBox tbTools;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbWeapons;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbArmor;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbSpecialName;
        private System.Windows.Forms.ComboBox cbClassName;
        private System.Windows.Forms.RichTextBox rtbSpecial;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnNewFeatures;
        private System.Windows.Forms.Button btnCreateClass;
    }
}