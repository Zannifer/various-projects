﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5eSpellbook
{
    class Spell
    {
        private string name;
        private int level;
        private bool ritual;
        private string school;
        private string castTime;
        private string components;
        private string duration;
        private string spell_range;
        private string description;
        private string classes;

        public Spell(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
        public int Level { get; set; }
        public bool Ritual { get; set; }
        public string School { get; set; }
        public string CastTime { get; set; }
        public string Components { get; set; }
        public string Duration { get; set; }
        public string SpellRange { get; set; }
        public string Description { get; set; }
        public string Classes { get; set; }        
    }
}
