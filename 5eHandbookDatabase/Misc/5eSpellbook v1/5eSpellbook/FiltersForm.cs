﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5eSpellbook
{
    public partial class FiltersForm : Form
    {        
        public delegate void TextAvailableEventHandler(string T);
        public event TextAvailableEventHandler TextAvailable;
        List<CheckBox> qCheckBox = new List<CheckBox>();
        List<ComboBox> qComboBox = new List<ComboBox>();

        public FiltersForm()
        {
            InitializeComponent();
           // CenterGroupBoxTitle(groupBox1);
        }
        /*  private void CenterGroupBoxTitle(GroupBox groupbox)
          {
              Label label = new Label();
              label.Text = groupbox.Text;
              groupbox.Text = "";
              label.Left = groupbox.Left + (groupbox.Width - label.Width) / 4;
              label.Top = groupbox.Top + 2; // 2 is an example : adjust the constant
              label.Parent = groupbox.Parent;
              label.BringToFront();
          }
          */
       

        private void FiltersForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Cancel Button 
            this.Close();
                   
        }
       
        private void button3_Click(object sender, EventArgs e)
        {
            //Filter Results
           
            if(!cbxAbj.Checked && !cbxCon.Checked && !cbxConj.Checked && !cbxRitual.Checked && !cbxDiv.Checked
                && !cbxEnchant.Checked && !cbxEvoc.Checked && !cbxIllusion.Checked && !cbxNecro.Checked 
                && !cbxTrans.Checked && cbClasses.SelectedIndex == 0 && cbLevels.SelectedIndex == 0)
            {
                MessageBox.Show("Need to have at least one item selected.");
            }
            else { 
                string query = "SELECT spell_name from handbook.spellbook ";

                query = queryBox(cbxCon, true, query);
                query = queryBox(cbxRitual, true, query);
                query = queryBox(cbxAbj, cbxAbj.Checked, query);
                query = queryBox(cbxConj, cbxConj.Checked, query);
                query = queryBox(cbxDiv, cbxDiv.Checked, query);
                query = queryBox(cbxEnchant, cbxEnchant.Checked, query);
                query = queryBox(cbxEvoc, cbxEvoc.Checked, query);
                query = queryBox(cbxIllusion, cbxIllusion.Checked, query);
                query = queryBox(cbxNecro, cbxNecro.Checked, query);                
                query = queryBox(cbxTrans, cbxTrans.Checked, query);               


                MessageBox.Show(query);
                TextAvailable(query);

                this.Hide();
            }
            
        }

        public string queryBox(Control c, bool b, string s)
        {
            string temp = s;
            if (!b)
            {
                return temp;
            }
            
            if (c is ComboBox)  //level && class comboboxes
            {
                if (((ComboBox)c).SelectedIndex == 0)
                {
                    return temp;
                }
                else  //check for which ComboBox
                {
                    if (((ComboBox)c).Items.Contains("All Classes"))  //class ComboBox
                    {
                        if (s.Contains("WHERE"))
                        {
                            temp += "AND class like '%" + ((ComboBox)c).SelectedItem.ToString() + "%' ";
                        }
                        else
                        {
                            temp += "WHERE class like '%" + ((ComboBox)c).SelectedItem.ToString() + "%' ";

                        }
                    }
                    if (((ComboBox)c).Items.Contains("All Levels")) //level ComboBox
                    {
                        if (s.Contains("WHERE"))
                        {
                            temp += "AND spell_level = " + Convert.ToInt32(((ComboBox)c).SelectedItem.ToString());
                        }
                        else
                        {
                            temp += "WHERE spell_level = " + Convert.ToInt32(((ComboBox)c).SelectedItem.ToString());
                        }
                    }
                    
                }

            }
            if (c is CheckBox && ((CheckBox)c).Checked)
            {
                if (((CheckBox)c).Text == "Ritual") //Ritual checkbox
                {
                    if (s.Contains("WHERE"))
                    {
                        temp += "AND ritual = true ";
                    }
                    else
                    {
                        temp += "WHERE ritual = true ";
                    }
                }
                else if (((CheckBox)c).Text == "Concentration")  //Concentration checkbox
                {
                    if (s.Contains("WHERE"))
                    {
                        temp += "AND duration like 'Concentration%' ";
                    }
                    else
                    {
                        temp += "WHERE duration like 'Concentration%' ";
                    }
                }
                else
                {
                    if (s.Contains("WHERE"))
                    {
                        temp += "OR school = '" + ((CheckBox)c).Text + "' ";  //School checkboxes
                    }
                    else
                    {
                        temp += "WHERE school = '" + ((CheckBox)c).Text + "' ";
                    }
                } 
            }
            else
            {
                return temp;
            }
            return temp;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //Clear Filters
            cbxAbj.Checked = false;
            cbxCon.Checked = false;
            cbxConj.Checked = false;
            cbxRitual.Checked = false;
            cbxDiv.Checked = false;
            cbxEnchant.Checked = false;
            cbxEvoc.Checked = false;
            cbxIllusion.Checked = false;
            cbxNecro.Checked = false;
            cbxTrans.Checked = false;
            cbClasses.SelectedIndex = 0;
            cbLevels.SelectedIndex = 0;
        }
    }
}
