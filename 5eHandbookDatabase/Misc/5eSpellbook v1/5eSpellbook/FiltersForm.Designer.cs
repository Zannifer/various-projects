﻿namespace _5eSpellbook
{
    partial class FiltersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbClasses = new System.Windows.Forms.ComboBox();
            this.cbLevels = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cbxRitual = new System.Windows.Forms.CheckBox();
            this.cbxCon = new System.Windows.Forms.CheckBox();
            this.cbxAbj = new System.Windows.Forms.CheckBox();
            this.cbxDiv = new System.Windows.Forms.CheckBox();
            this.cbxEvoc = new System.Windows.Forms.CheckBox();
            this.cbxNecro = new System.Windows.Forms.CheckBox();
            this.cbxConj = new System.Windows.Forms.CheckBox();
            this.cbxEnchant = new System.Windows.Forms.CheckBox();
            this.cbxIllusion = new System.Windows.Forms.CheckBox();
            this.cbxTrans = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbClasses
            // 
            this.cbClasses.FormattingEnabled = true;
            this.cbClasses.Items.AddRange(new object[] {
            "All Classes",
            "Barbarian",
            "Bard",
            "Cleric",
            "Druid",
            "Fighter",
            "Monk",
            "Paladin",
            "Ranger",
            "Rogue",
            "Sorcerer",
            "Warlock",
            "Wizard"});
            this.cbClasses.Location = new System.Drawing.Point(12, 33);
            this.cbClasses.Name = "cbClasses";
            this.cbClasses.Size = new System.Drawing.Size(121, 21);
            this.cbClasses.TabIndex = 0;
            this.cbClasses.Text = "All Classes";
            // 
            // cbLevels
            // 
            this.cbLevels.FormattingEnabled = true;
            this.cbLevels.Items.AddRange(new object[] {
            "All Levels",
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.cbLevels.Location = new System.Drawing.Point(151, 33);
            this.cbLevels.Name = "cbLevels";
            this.cbLevels.Size = new System.Drawing.Size(121, 21);
            this.cbLevels.TabIndex = 1;
            this.cbLevels.Text = "All Levels";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(12, 276);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 36);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "CANCEL";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(105, 276);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 36);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(197, 276);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 36);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "FILTER RESULTS";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.button3_Click);
            // 
            // cbxRitual
            // 
            this.cbxRitual.AutoSize = true;
            this.cbxRitual.Location = new System.Drawing.Point(67, 80);
            this.cbxRitual.Name = "cbxRitual";
            this.cbxRitual.Size = new System.Drawing.Size(53, 17);
            this.cbxRitual.TabIndex = 5;
            this.cbxRitual.Text = "Ritual";
            this.cbxRitual.UseVisualStyleBackColor = true;
            // 
            // cbxCon
            // 
            this.cbxCon.AutoSize = true;
            this.cbxCon.Location = new System.Drawing.Point(138, 80);
            this.cbxCon.Name = "cbxCon";
            this.cbxCon.Size = new System.Drawing.Size(92, 17);
            this.cbxCon.TabIndex = 6;
            this.cbxCon.Text = "Concentration";
            this.cbxCon.UseVisualStyleBackColor = true;
            // 
            // cbxAbj
            // 
            this.cbxAbj.AutoSize = true;
            this.cbxAbj.Location = new System.Drawing.Point(7, 30);
            this.cbxAbj.Name = "cbxAbj";
            this.cbxAbj.Size = new System.Drawing.Size(73, 17);
            this.cbxAbj.TabIndex = 7;
            this.cbxAbj.Text = "Abjuration";
            this.cbxAbj.UseVisualStyleBackColor = true;
            // 
            // cbxDiv
            // 
            this.cbxDiv.AutoSize = true;
            this.cbxDiv.Location = new System.Drawing.Point(7, 53);
            this.cbxDiv.Name = "cbxDiv";
            this.cbxDiv.Size = new System.Drawing.Size(73, 17);
            this.cbxDiv.TabIndex = 8;
            this.cbxDiv.Text = "Divination";
            this.cbxDiv.UseVisualStyleBackColor = true;
            // 
            // cbxEvoc
            // 
            this.cbxEvoc.AutoSize = true;
            this.cbxEvoc.Location = new System.Drawing.Point(6, 76);
            this.cbxEvoc.Name = "cbxEvoc";
            this.cbxEvoc.Size = new System.Drawing.Size(74, 17);
            this.cbxEvoc.TabIndex = 9;
            this.cbxEvoc.Text = "Evocation";
            this.cbxEvoc.UseVisualStyleBackColor = true;
            // 
            // cbxNecro
            // 
            this.cbxNecro.AutoSize = true;
            this.cbxNecro.Location = new System.Drawing.Point(6, 99);
            this.cbxNecro.Name = "cbxNecro";
            this.cbxNecro.Size = new System.Drawing.Size(86, 17);
            this.cbxNecro.TabIndex = 10;
            this.cbxNecro.Text = "Necromancy";
            this.cbxNecro.UseVisualStyleBackColor = true;
            // 
            // cbxConj
            // 
            this.cbxConj.AutoSize = true;
            this.cbxConj.Location = new System.Drawing.Point(109, 30);
            this.cbxConj.Name = "cbxConj";
            this.cbxConj.Size = new System.Drawing.Size(79, 17);
            this.cbxConj.TabIndex = 11;
            this.cbxConj.Text = "Conjuration";
            this.cbxConj.UseVisualStyleBackColor = true;
            // 
            // cbxEnchant
            // 
            this.cbxEnchant.AutoSize = true;
            this.cbxEnchant.Location = new System.Drawing.Point(109, 53);
            this.cbxEnchant.Name = "cbxEnchant";
            this.cbxEnchant.Size = new System.Drawing.Size(89, 17);
            this.cbxEnchant.TabIndex = 12;
            this.cbxEnchant.Text = "Enchantment";
            this.cbxEnchant.UseVisualStyleBackColor = true;
            // 
            // cbxIllusion
            // 
            this.cbxIllusion.AutoSize = true;
            this.cbxIllusion.Location = new System.Drawing.Point(110, 76);
            this.cbxIllusion.Name = "cbxIllusion";
            this.cbxIllusion.Size = new System.Drawing.Size(58, 17);
            this.cbxIllusion.TabIndex = 13;
            this.cbxIllusion.Text = "Illusion";
            this.cbxIllusion.UseVisualStyleBackColor = true;
            // 
            // cbxTrans
            // 
            this.cbxTrans.AutoSize = true;
            this.cbxTrans.Location = new System.Drawing.Point(109, 99);
            this.cbxTrans.Name = "cbxTrans";
            this.cbxTrans.Size = new System.Drawing.Size(93, 17);
            this.cbxTrans.TabIndex = 14;
            this.cbxTrans.Text = "Transmutation";
            this.cbxTrans.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxAbj);
            this.groupBox1.Controls.Add(this.cbxTrans);
            this.groupBox1.Controls.Add(this.cbxConj);
            this.groupBox1.Controls.Add(this.cbxIllusion);
            this.groupBox1.Controls.Add(this.cbxDiv);
            this.groupBox1.Controls.Add(this.cbxEnchant);
            this.groupBox1.Controls.Add(this.cbxEvoc);
            this.groupBox1.Controls.Add(this.cbxNecro);
            this.groupBox1.Location = new System.Drawing.Point(28, 115);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 135);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Schools";
            // 
            // FiltersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 351);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cbxCon);
            this.Controls.Add(this.cbxRitual);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.cbLevels);
            this.Controls.Add(this.cbClasses);
            this.Name = "FiltersForm";
            this.Text = "Filters";
            this.Load += new System.EventHandler(this.FiltersForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbClasses;
        private System.Windows.Forms.ComboBox cbLevels;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox cbxRitual;
        private System.Windows.Forms.CheckBox cbxCon;
        private System.Windows.Forms.CheckBox cbxAbj;
        private System.Windows.Forms.CheckBox cbxDiv;
        private System.Windows.Forms.CheckBox cbxEvoc;
        private System.Windows.Forms.CheckBox cbxNecro;
        private System.Windows.Forms.CheckBox cbxConj;
        private System.Windows.Forms.CheckBox cbxEnchant;
        private System.Windows.Forms.CheckBox cbxIllusion;
        private System.Windows.Forms.CheckBox cbxTrans;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}