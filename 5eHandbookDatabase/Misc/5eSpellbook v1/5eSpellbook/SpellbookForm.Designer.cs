﻿namespace _5eSpellbook
{
    partial class SpellbookForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxSpellNames = new System.Windows.Forms.ListBox();
            this.btnRemove = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cbWizard = new System.Windows.Forms.CheckBox();
            this.cbWarlock = new System.Windows.Forms.CheckBox();
            this.cbSorcerer = new System.Windows.Forms.CheckBox();
            this.cbRanger = new System.Windows.Forms.CheckBox();
            this.cbPaladin = new System.Windows.Forms.CheckBox();
            this.cbDruid = new System.Windows.Forms.CheckBox();
            this.cbCleric = new System.Windows.Forms.CheckBox();
            this.cbBard = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rtbDescription = new System.Windows.Forms.RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbDuration = new System.Windows.Forms.TextBox();
            this.tbComponents = new System.Windows.Forms.TextBox();
            this.tbRange = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbRitual = new System.Windows.Forms.CheckBox();
            this.cbLevel = new System.Windows.Forms.ComboBox();
            this.cbSchool = new System.Windows.Forms.ComboBox();
            this.tbCastTime = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.rtbDisplaySpell = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.filtersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rBtnAlpha = new System.Windows.Forms.RadioButton();
            this.rBtnPhrase = new System.Windows.Forms.RadioButton();
            this.rBtnText = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbxSpellNames
            // 
            this.lbxSpellNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxSpellNames.FormattingEnabled = true;
            this.lbxSpellNames.ItemHeight = 16;
            this.lbxSpellNames.Location = new System.Drawing.Point(3, 83);
            this.lbxSpellNames.Name = "lbxSpellNames";
            this.lbxSpellNames.Size = new System.Drawing.Size(225, 340);
            this.lbxSpellNames.TabIndex = 0;
            this.lbxSpellNames.SelectedIndexChanged += new System.EventHandler(this.lbxSpellNames_SelectedIndexChanged);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(53, 429);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(125, 45);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "REMOVE SPELL";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rBtnText);
            this.panel1.Controls.Add(this.rBtnPhrase);
            this.panel1.Controls.Add(this.rBtnAlpha);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.lbxSpellNames);
            this.panel1.Controls.Add(this.btnRemove);
            this.panel1.Location = new System.Drawing.Point(10, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(231, 487);
            this.panel1.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(2, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(226, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.rtbDisplaySpell);
            this.panel2.Location = new System.Drawing.Point(247, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(743, 487);
            this.panel2.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAdd);
            this.groupBox2.Controls.Add(this.cbWizard);
            this.groupBox2.Controls.Add(this.cbWarlock);
            this.groupBox2.Controls.Add(this.cbSorcerer);
            this.groupBox2.Controls.Add(this.cbRanger);
            this.groupBox2.Controls.Add(this.cbPaladin);
            this.groupBox2.Controls.Add(this.cbDruid);
            this.groupBox2.Controls.Add(this.cbCleric);
            this.groupBox2.Controls.Add(this.cbBard);
            this.groupBox2.Location = new System.Drawing.Point(619, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(116, 419);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Class Options";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(6, 353);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(104, 53);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "ADD SPELL";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cbWizard
            // 
            this.cbWizard.AutoSize = true;
            this.cbWizard.Location = new System.Drawing.Point(6, 196);
            this.cbWizard.Name = "cbWizard";
            this.cbWizard.Size = new System.Drawing.Size(70, 17);
            this.cbWizard.TabIndex = 8;
            this.cbWizard.Text = "WIZARD";
            this.cbWizard.UseVisualStyleBackColor = true;
            // 
            // cbWarlock
            // 
            this.cbWarlock.AutoSize = true;
            this.cbWarlock.Location = new System.Drawing.Point(6, 173);
            this.cbWarlock.Name = "cbWarlock";
            this.cbWarlock.Size = new System.Drawing.Size(80, 17);
            this.cbWarlock.TabIndex = 7;
            this.cbWarlock.Text = "WARLOCK";
            this.cbWarlock.UseVisualStyleBackColor = true;
            // 
            // cbSorcerer
            // 
            this.cbSorcerer.AutoSize = true;
            this.cbSorcerer.Location = new System.Drawing.Point(6, 150);
            this.cbSorcerer.Name = "cbSorcerer";
            this.cbSorcerer.Size = new System.Drawing.Size(86, 17);
            this.cbSorcerer.TabIndex = 6;
            this.cbSorcerer.Text = "SORCERER";
            this.cbSorcerer.UseVisualStyleBackColor = true;
            // 
            // cbRanger
            // 
            this.cbRanger.AutoSize = true;
            this.cbRanger.Location = new System.Drawing.Point(6, 127);
            this.cbRanger.Name = "cbRanger";
            this.cbRanger.Size = new System.Drawing.Size(72, 17);
            this.cbRanger.TabIndex = 5;
            this.cbRanger.Text = "RANGER";
            this.cbRanger.UseVisualStyleBackColor = true;
            // 
            // cbPaladin
            // 
            this.cbPaladin.AutoSize = true;
            this.cbPaladin.Location = new System.Drawing.Point(6, 104);
            this.cbPaladin.Name = "cbPaladin";
            this.cbPaladin.Size = new System.Drawing.Size(72, 17);
            this.cbPaladin.TabIndex = 4;
            this.cbPaladin.Text = "PALADIN";
            this.cbPaladin.UseVisualStyleBackColor = true;
            // 
            // cbDruid
            // 
            this.cbDruid.AutoSize = true;
            this.cbDruid.Location = new System.Drawing.Point(6, 81);
            this.cbDruid.Name = "cbDruid";
            this.cbDruid.Size = new System.Drawing.Size(61, 17);
            this.cbDruid.TabIndex = 3;
            this.cbDruid.Text = "DRUID";
            this.cbDruid.UseVisualStyleBackColor = true;
            // 
            // cbCleric
            // 
            this.cbCleric.AutoSize = true;
            this.cbCleric.Location = new System.Drawing.Point(6, 58);
            this.cbCleric.Name = "cbCleric";
            this.cbCleric.Size = new System.Drawing.Size(64, 17);
            this.cbCleric.TabIndex = 2;
            this.cbCleric.Text = "CLERIC";
            this.cbCleric.UseVisualStyleBackColor = true;
            // 
            // cbBard
            // 
            this.cbBard.AutoSize = true;
            this.cbBard.Location = new System.Drawing.Point(6, 35);
            this.cbBard.Name = "cbBard";
            this.cbBard.Size = new System.Drawing.Size(56, 17);
            this.cbBard.TabIndex = 1;
            this.cbBard.Text = "BARD";
            this.cbBard.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rtbDescription);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tbDuration);
            this.groupBox1.Controls.Add(this.tbComponents);
            this.groupBox1.Controls.Add(this.tbRange);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.cbRitual);
            this.groupBox1.Controls.Add(this.cbLevel);
            this.groupBox1.Controls.Add(this.cbSchool);
            this.groupBox1.Controls.Add(this.tbCastTime);
            this.groupBox1.Controls.Add(this.tbName);
            this.groupBox1.Location = new System.Drawing.Point(325, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 419);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SPELL ENTRY";
            // 
            // rtbDescription
            // 
            this.rtbDescription.Location = new System.Drawing.Point(6, 250);
            this.rtbDescription.Name = "rtbDescription";
            this.rtbDescription.Size = new System.Drawing.Size(275, 164);
            this.rtbDescription.TabIndex = 16;
            this.rtbDescription.Text = "";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "DESCRIPTION";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 206);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "DURATION";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "COMPONENTS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(60, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "RANGE";
            // 
            // tbDuration
            // 
            this.tbDuration.Location = new System.Drawing.Point(111, 203);
            this.tbDuration.Name = "tbDuration";
            this.tbDuration.Size = new System.Drawing.Size(170, 20);
            this.tbDuration.TabIndex = 11;
            // 
            // tbComponents
            // 
            this.tbComponents.Location = new System.Drawing.Point(111, 177);
            this.tbComponents.Name = "tbComponents";
            this.tbComponents.Size = new System.Drawing.Size(170, 20);
            this.tbComponents.TabIndex = 10;
            // 
            // tbRange
            // 
            this.tbRange.Location = new System.Drawing.Point(111, 151);
            this.tbRange.Name = "tbRange";
            this.tbRange.Size = new System.Drawing.Size(170, 20);
            this.tbRange.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "CASTING TIME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "LEVEL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "SPELL SCHOOL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "SPELL NAME";
            // 
            // cbRitual
            // 
            this.cbRitual.AutoSize = true;
            this.cbRitual.Location = new System.Drawing.Point(111, 102);
            this.cbRitual.Name = "cbRitual";
            this.cbRitual.Size = new System.Drawing.Size(93, 17);
            this.cbRitual.TabIndex = 4;
            this.cbRitual.Text = "Is this a ritual?";
            this.cbRitual.UseVisualStyleBackColor = true;
            // 
            // cbLevel
            // 
            this.cbLevel.FormattingEnabled = true;
            this.cbLevel.Location = new System.Drawing.Point(111, 74);
            this.cbLevel.Name = "cbLevel";
            this.cbLevel.Size = new System.Drawing.Size(95, 21);
            this.cbLevel.TabIndex = 3;
            // 
            // cbSchool
            // 
            this.cbSchool.FormattingEnabled = true;
            this.cbSchool.Location = new System.Drawing.Point(111, 46);
            this.cbSchool.Name = "cbSchool";
            this.cbSchool.Size = new System.Drawing.Size(170, 21);
            this.cbSchool.TabIndex = 2;
            // 
            // tbCastTime
            // 
            this.tbCastTime.Location = new System.Drawing.Point(111, 125);
            this.tbCastTime.Name = "tbCastTime";
            this.tbCastTime.Size = new System.Drawing.Size(170, 20);
            this.tbCastTime.TabIndex = 1;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(111, 19);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(170, 20);
            this.tbName.TabIndex = 0;
            // 
            // rtbDisplaySpell
            // 
            this.rtbDisplaySpell.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbDisplaySpell.Location = new System.Drawing.Point(4, 4);
            this.rtbDisplaySpell.Name = "rtbDisplaySpell";
            this.rtbDisplaySpell.Size = new System.Drawing.Size(314, 419);
            this.rtbDisplaySpell.TabIndex = 0;
            this.rtbDisplaySpell.Text = "";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.filtersToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(999, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // filtersToolStripMenuItem
            // 
            this.filtersToolStripMenuItem.Name = "filtersToolStripMenuItem";
            this.filtersToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.filtersToolStripMenuItem.Text = "Filters";
            this.filtersToolStripMenuItem.Click += new System.EventHandler(this.filtersToolStripMenuItem_Click);
            // 
            // rBtnAlpha
            // 
            this.rBtnAlpha.AutoSize = true;
            this.rBtnAlpha.Location = new System.Drawing.Point(4, 26);
            this.rBtnAlpha.Name = "rBtnAlpha";
            this.rBtnAlpha.Size = new System.Drawing.Size(120, 17);
            this.rBtnAlpha.TabIndex = 3;
            this.rBtnAlpha.TabStop = true;
            this.rBtnAlpha.Text = "Alphabetical Search";
            this.rBtnAlpha.UseVisualStyleBackColor = true;
            // 
            // rBtnPhrase
            // 
            this.rBtnPhrase.AutoSize = true;
            this.rBtnPhrase.Location = new System.Drawing.Point(4, 44);
            this.rBtnPhrase.Name = "rBtnPhrase";
            this.rBtnPhrase.Size = new System.Drawing.Size(90, 17);
            this.rBtnPhrase.TabIndex = 4;
            this.rBtnPhrase.TabStop = true;
            this.rBtnPhrase.Text = "Name Search";
            this.rBtnPhrase.UseVisualStyleBackColor = true;
            // 
            // rBtnText
            // 
            this.rBtnText.AutoSize = true;
            this.rBtnText.Location = new System.Drawing.Point(4, 62);
            this.rBtnText.Name = "rBtnText";
            this.rBtnText.Size = new System.Drawing.Size(83, 17);
            this.rBtnText.TabIndex = 5;
            this.rBtnText.TabStop = true;
            this.rBtnText.Text = "Text Search";
            this.rBtnText.UseVisualStyleBackColor = true;
            // 
            // SpellbookForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 528);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SpellbookForm";
            this.Text = "Spellbook";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxSpellNames;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RichTextBox rtbDisplaySpell;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbRitual;
        private System.Windows.Forms.ComboBox cbLevel;
        private System.Windows.Forms.ComboBox cbSchool;
        private System.Windows.Forms.TextBox tbCastTime;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox rtbDescription;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbDuration;
        private System.Windows.Forms.TextBox tbComponents;
        private System.Windows.Forms.TextBox tbRange;
        private System.Windows.Forms.CheckBox cbWizard;
        private System.Windows.Forms.CheckBox cbWarlock;
        private System.Windows.Forms.CheckBox cbSorcerer;
        private System.Windows.Forms.CheckBox cbRanger;
        private System.Windows.Forms.CheckBox cbPaladin;
        private System.Windows.Forms.CheckBox cbDruid;
        private System.Windows.Forms.CheckBox cbCleric;
        private System.Windows.Forms.CheckBox cbBard;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem filtersToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton rBtnPhrase;
        private System.Windows.Forms.RadioButton rBtnAlpha;
        private System.Windows.Forms.RadioButton rBtnText;
    }
}

