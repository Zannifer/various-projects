﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace _5ePlayersHandbook
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            loadSpellNames("Select spell_name from handbook.spellbook order by spell_name");
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            toolStripTextBox1.Clear();
        }

        private void loadSpellNames(string s)
        {
            lbxSpellNames.Items.Clear();
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand(s, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    lbxSpellNames.Items.Add(rdr.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }

        private string insertSQLEscape(string s)
        {
            string temp = "";
            string[] result = Regex.Split(s, @"\'");
            for (int i = 0; i < result.Length; i++)
            {
                if (i < result.Length - 1)
                {
                    temp += result[i] + "\\\'";
                }
                else
                {
                    temp += result[i];
                }
            }
            return temp;
        }

        private void lbxSpellNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            rtbDisplaySpell.Clear();
            string name = insertSQLEscape(lbxSpellNames.SelectedItem.ToString());
            string result = "";
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                string stm = "Select * from handbook.spellbook where spell_name = '" + name + "'";
                MySqlCommand cmd = new MySqlCommand(stm, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (rdr.GetInt32(1) == 0)
                    {
                        result += rdr.GetString(0) + "\n" + rdr.GetString(3) + " cantrip";
                    }
                    else if (rdr.GetInt32(1) == 1)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3);
                        }
                    }
                    else if (rdr.GetInt32(1) == 2)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3);
                        }
                    }
                    else if (rdr.GetInt32(1) == 3)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3);
                        }
                    }
                    else
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3);
                        }
                    }
                    result += "\nCasting Time: " + rdr.GetString(4);
                    result += "\nRange: " + rdr.GetString(7);
                    result += "\nComponents: " + rdr.GetString(5);
                    result += "\nDuration: " + rdr.GetString(6);
                    result += "\n\n" + rdr.GetString(8);
                }
                rtbDisplaySpell.Text = result;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }

        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            string searchQuery = toolStripTextBox1.Text;
            loadSpellNames("Select spell_name from handbook.spellbook where spell_name like '%" + searchQuery + "%' OR description like '%" + searchQuery + "%' order by spell_name");
        }

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadSpellNames("Select spell_name from handbook.spellbook order by spell_name");
            rtbDisplaySpell.Clear();
        }
    }
}
