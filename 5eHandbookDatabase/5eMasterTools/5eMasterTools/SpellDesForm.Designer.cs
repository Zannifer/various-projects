﻿namespace _5eMasterTools
{
    partial class SpellDesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReturn = new System.Windows.Forms.Button();
            this.tbSpellDes = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(331, 537);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(75, 23);
            this.btnReturn.TabIndex = 0;
            this.btnReturn.Text = "Return Text";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // tbSpellDes
            // 
            this.tbSpellDes.Location = new System.Drawing.Point(12, 12);
            this.tbSpellDes.Name = "tbSpellDes";
            this.tbSpellDes.Size = new System.Drawing.Size(394, 519);
            this.tbSpellDes.TabIndex = 1;
            this.tbSpellDes.Text = "";
            // 
            // SpellDesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 572);
            this.Controls.Add(this.tbSpellDes);
            this.Controls.Add(this.btnReturn);
            this.Name = "SpellDesForm";
            this.Text = "Description";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReturn;
        public System.Windows.Forms.RichTextBox tbSpellDes;
    }
}