﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5eMasterTools
{
    class Spells
    {

        public string SpellName { get; set; }
        public int Level { get; set; }
        public bool Ritual { get; set; }
        public string School { get; set; }
        public string CastTime { get; set; }
        public string Components { get; set; }
        public string Duration { get; set; }
        public string Range { get; set; }
        public string Description { get; set; }
        public string Classes { get; set; }

    }
}
