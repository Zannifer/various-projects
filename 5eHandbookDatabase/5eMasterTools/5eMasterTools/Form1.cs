﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace _5eMasterTools
{
    public partial class Form1 : Form
    {
        SpellDesForm Frm2 = new SpellDesForm();
        SpellFilterForm Frm3 = new SpellFilterForm();
        
        private string searchQuery;
        public static bool filters = false;
        public static bool isDForm = false;
        public Form1()
        {
            InitializeComponent();
            Frm2.TextAvailable += new SpellDesForm.TextAvailableEventHandler(Frm2_TextAvailable);
            Frm3.TextAvailable += new SpellFilterForm.TextAvailableEventHandler(Frm3_TextAvailable);
            loadSpellNames("Select spell_name from handbook.spellbook order by spell_name");
            loadSpellLevels();
            loadSchoolNames();
        } 
        
        public string Server { get; set; }
        public string Database { get; set; }
        public string User { get; set; }
        public string Password { get; set; }      

        private void loadSchoolNames()
        {
            cbSchool.Items.Add("Abjuration");
            cbSchool.Items.Add("Conjuration");
            cbSchool.Items.Add("Divination");
            cbSchool.Items.Add("Enchantment");
            cbSchool.Items.Add("Evocation");
            cbSchool.Items.Add("Illusion");
            cbSchool.Items.Add("Necromancy");
            cbSchool.Items.Add("Transmutation");
        }
        private void loadSpellLevels()
        {
            cbLevel.Items.Add("0");
            cbLevel.Items.Add("1");
            cbLevel.Items.Add("2");
            cbLevel.Items.Add("3");
            cbLevel.Items.Add("4");
            cbLevel.Items.Add("5");
            cbLevel.Items.Add("6");
            cbLevel.Items.Add("7");
            cbLevel.Items.Add("8");
            cbLevel.Items.Add("9");
        }      

        private void loadSpellNames(string s)
        {
            lbxSpellNames.Items.Clear();
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand(s, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    lbxSpellNames.Items.Add(rdr.GetString(0));
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }        

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            searchQuery = insertSQLEscape(tbSearch.Text);
            if (rBtnAlpha.Checked)
            {
                loadSpellNames("Select spell_name from handbook.spellbook where spell_name like '" + searchQuery + "%' order by spell_name");
            }
            if (rBtnPhrase.Checked)
            {
                loadSpellNames("Select spell_name from handbook.spellbook where spell_name like '%" + searchQuery + "%' order by spell_name");
            }
            if (rBtnText.Checked)
            {
                loadSpellNames("Select spell_name from handbook.spellbook where description like '%" + searchQuery + "%' order by spell_name");
            }
        }

        private string insertSQLEscape(string s)
        {
            string temp = "";
            string[] result = Regex.Split(s, @"\'");
            for (int i = 0; i < result.Length; i++)
            {
                if (i < result.Length - 1)
                {
                    temp += result[i] + "\\\'";
                }
                else
                {
                    temp += result[i];
                }
            }
            return temp;
        }

        private void lbxSpellNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            rtbDisplaySpell.Clear();
            string name = insertSQLEscape(lbxSpellNames.SelectedItem.ToString());
            string result = "";
            MySqlConnection cnn = null;
            MySqlDataReader rdr = null;
            string connectionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            try
            {
                cnn = new MySqlConnection(connectionString);
                cnn.Open();
                string stm = "Select * from handbook.spellbook where spell_name = '" + name + "'";
                MySqlCommand cmd = new MySqlCommand(stm, cnn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (rdr.GetInt32(1) == 0)
                    {
                        result += rdr.GetString(0) + "\n" + rdr.GetString(3) + " cantrip";
                    }
                    else if (rdr.GetInt32(1) == 1)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "st-level " + rdr.GetString(3);
                        }
                    }
                    else if (rdr.GetInt32(1) == 2)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "nd-level " + rdr.GetString(3);
                        }
                    }
                    else if (rdr.GetInt32(1) == 3)
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "rd-level " + rdr.GetString(3);
                        }
                    }
                    else
                    {
                        if (rdr.GetBoolean(2))
                        {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3) + " (Ritual)";
                        }
                        else {
                            result += rdr.GetString(0) + "\n" + rdr.GetInt32(1) + "th-level " + rdr.GetString(3);
                        }
                    }
                    result += "\nCasting Time: " + rdr.GetString(4);
                    result += "\nRange: " + rdr.GetString(7);
                    result += "\nComponents: " + rdr.GetString(5);
                    result += "\nDuration: " + rdr.GetString(6);
                    result += "\n\n" + rdr.GetString(8);
                }
                rtbDisplaySpell.Text = result;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error: {0}", ex.ToString());
            }
            finally
            {
                if (rdr != null)
                {
                    rdr.Close();
                }
                if (cnn != null)
                {
                    cnn.Close();
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            string connetionString = null;
            MySqlConnection cnn;
            connetionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
            cnn = new MySqlConnection(connetionString);
            cnn.Open();
            using (MySqlCommand cmd = new MySqlCommand("DELETE FROM handbook.spellbook WHERE spell_name = '" + lbxSpellNames.SelectedItem.ToString() + "'", cnn))
            {
                try
                {
                    MySqlDataReader rdr = cmd.ExecuteReader();
                }
                catch (Exception ex)
                {
                    string s = String.Format("Error: {0}", ex.ToString());
                    MessageBox.Show(s);
                }
                finally
                {
                    loadSpellNames("Select spell_name from handbook.spellbook order by spell_name");
                    rtbDisplaySpell.Clear();
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string classString = "";
            if (cbBard.Checked)
            {
                classString += "Bard ";
                cbBard.Checked = false;
            }
            if (cbCleric.Checked)
            {
                classString += "Cleric ";
                cbCleric.Checked = false;
            }
            if (cbDruid.Checked)
            {
                classString += "Druid ";
                cbDruid.Checked = false;
            }
            if (cbPaladin.Checked)
            {
                classString += "Paladin ";
                cbPaladin.Checked = false;
            }
            if (cbRanger.Checked)
            {
                classString += "Ranger ";
                cbRanger.Checked = false;
            }
            if (cbSorcerer.Checked)
            {
                classString += "Sorcerer ";
                cbSorcerer.Checked = false;
            }
            if (cbWarlock.Checked)
            {
                classString += "Warlock ";
                cbWarlock.Checked = false;
            }
            if (cbWizard.Checked)
            {
                classString += "Wizard ";
                cbWizard.Checked = false;
            }

            try
            {
                string connetionString = null;
                MySqlConnection cnn;
                connetionString = "server=localhost;database=handbook;uid=Zannifer;pwd=deltA1979;";
                cnn = new MySqlConnection(connetionString);
                cnn.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = cnn;
                cmd.CommandText = "INSERT INTO handbook.spellbook(spell_name, spell_level, ritual, school, cast_time, components, duration, spell_range, description, class) VALUES(@spell_name, @spell_level, @ritual, @school, @cast_time, @components, @duration, @spell_range, @description, @class)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@spell_name", tbName.Text);
                cmd.Parameters.AddWithValue("@spell_level", Convert.ToInt32(cbLevel.SelectedItem));
                cmd.Parameters.AddWithValue("@ritual", cbRitual.Checked);
                cmd.Parameters.AddWithValue("@school", cbSchool.SelectedItem);
                cmd.Parameters.AddWithValue("@cast_time", tbCastTime.Text);
                cmd.Parameters.AddWithValue("@components", tbComponents.Text);
                cmd.Parameters.AddWithValue("@duration", tbDuration.Text);
                cmd.Parameters.AddWithValue("@spell_range", tbRange.Text);
                cmd.Parameters.AddWithValue("@description", rtbDescription.Text);
                cmd.Parameters.AddWithValue("@class", classString);

                cmd.ExecuteNonQuery();

                cnn.Close();
                loadSpellNames("Select spell_name from handbook.spellbook order by spell_name");
            }
            catch (Exception ex)
            {
                string s = String.Format("Error: {0}", ex.ToString());
                MessageBox.Show(s);
            }
            finally
            {
                tbName.Clear();
                cbRitual.Checked = false;
                tbCastTime.Clear();
                tbComponents.Clear();
                tbDuration.Clear();
                tbRange.Clear();
                rtbDescription.Clear();
                cbLevel.SelectedIndex = -1;
                cbSchool.SelectedIndex = -1;

            }
        }

        private void Frm2_TextAvailable(string T)
        {
            rtbDescription.Text = T;
        }

        private void Frm3_TextAvailable(string T)
        {
            loadSpellNames(T);
        }

        private void spellFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Frm3.Show();
        }

        private void rtbDescription_DoubleClick(object sender, EventArgs e)
        {
            Frm2.Show();
            Frm2.tbSpellDes.Text = rtbDescription.Text;
        }
    }
}
