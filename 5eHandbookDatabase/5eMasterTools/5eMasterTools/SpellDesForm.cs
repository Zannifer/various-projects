﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _5eMasterTools
{
    public partial class SpellDesForm : Form
    {
        public delegate void TextAvailableEventHandler(string T);
        public event TextAvailableEventHandler TextAvailable;
        public SpellDesForm()
        {
            InitializeComponent();
        }

        private void btnReturn_Click(object sender, EventArgs e)
        {
            TextAvailable(tbSpellDes.Text);
            this.Close();
        }
    }
}
