﻿namespace _5eMasterTools
{
    partial class RaceDesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnReturn = new System.Windows.Forms.Button();
            this.tbRaceDes = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnReturn
            // 
            this.btnReturn.Location = new System.Drawing.Point(331, 537);
            this.btnReturn.Name = "btnReturn";
            this.btnReturn.Size = new System.Drawing.Size(75, 23);
            this.btnReturn.TabIndex = 1;
            this.btnReturn.Text = "Return Text";
            this.btnReturn.UseVisualStyleBackColor = true;
            this.btnReturn.Click += new System.EventHandler(this.btnReturn_Click);
            // 
            // tbRaceDes
            // 
            this.tbRaceDes.Location = new System.Drawing.Point(12, 12);
            this.tbRaceDes.Name = "tbRaceDes";
            this.tbRaceDes.Size = new System.Drawing.Size(394, 519);
            this.tbRaceDes.TabIndex = 2;
            this.tbRaceDes.Text = "";
            // 
            // RaceDesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 572);
            this.Controls.Add(this.tbRaceDes);
            this.Controls.Add(this.btnReturn);
            this.Name = "RaceDesForm";
            this.Text = "RaceDesForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnReturn;
        private System.Windows.Forms.RichTextBox tbRaceDes;
    }
}