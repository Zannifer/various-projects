﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dice
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        int sides = 4;
        int sum;
        int number = 1;
        int counter = 0;
        public Form1()
        {
            InitializeComponent();
            label3.Text = "";
            
        }
        
        private void button1_Click(object sender, EventArgs e)
        {           
            sum = 0;
			
            timer1.Start();
			   
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label3.Text = "";
            timer1.Stop();
            timer1.Dispose();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (listBox1.SelectedIndex)
            {
                case 0: sides = 4; break;
                case 1: sides = 6; break;
                case 2: sides = 8; break;
                case 3: sides = 10; break;
                case 4: sides = 20; break;
                case 5: sides = 100; break;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label3.Text = Convert.ToString(r.Next(1, sides));
            counter++;
            if (counter >= 10)
            {
                timer1.Stop();
                int roll = 0;

                for (int i = 0; i < number; i++)
                {
                    roll = r.Next(1, sides);
                    sum += roll;
                }
                label3.Text = String.Format("{0}", sum);
                counter = 0;
            }
        }

        private void numberListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            number = 1 + numberListBox.SelectedIndex;            
        }
    }
}
