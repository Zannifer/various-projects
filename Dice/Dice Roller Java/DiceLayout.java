import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DiceLayout extends JFrame {
	private int nSides = 1;
	private int number = 1;
	private int diceResult = 0;

	private TextArea results = new TextArea();
  	public DiceLayout() {

    	JPanel p1 = new JPanel();
    	p1.setLayout(new FlowLayout());

    	JRadioButton d2 = new JRadioButton("d2");
    	JRadioButton d4 = new JRadioButton("d4");
    	JRadioButton d6 = new JRadioButton("d6");
    	JRadioButton d8 = new JRadioButton("d8");
    	JRadioButton d10 = new JRadioButton("d10");
    	JRadioButton d12 = new JRadioButton("d12");
    	JRadioButton d20 = new JRadioButton("d20");
    	JRadioButton d100 = new JRadioButton("d100");

    	ButtonGroup group1 = new ButtonGroup();
    	group1.add(d2);
    	group1.add(d4);
    	group1.add(d6);
    	group1.add(d8);
    	group1.add(d10);
    	group1.add(d12);
    	group1.add(d20);
    	group1.add(d100);

    	p1.add(new JLabel("Dice Type"));

    	p1.add(d2);
    	p1.add(d4);
    	p1.add(d6);
    	p1.add(d8);
    	p1.add(d10);
    	p1.add(d12);
    	p1.add(d20);
    	p1.add(d100);

    	JPanel p2 = new JPanel();
    	p2.setLayout(new FlowLayout());

    	JRadioButton buttonNumber1 = new JRadioButton("1");
    	JRadioButton buttonNumber2 = new JRadioButton("2");
    	JRadioButton buttonNumber3 = new JRadioButton("3");
    	JRadioButton buttonNumber4 = new JRadioButton("4");
    	JRadioButton buttonNumber5 = new JRadioButton("5");
    	JRadioButton buttonNumber6 = new JRadioButton("6");
    	JRadioButton buttonNumber7 = new JRadioButton("7");
    	JRadioButton buttonNumber8 = new JRadioButton("8");
    	JRadioButton buttonNumber9 = new JRadioButton("9");
    	JRadioButton buttonNumber10 = new JRadioButton("10");

    	ButtonGroup group2 = new ButtonGroup();
    	group2.add(buttonNumber1);
    	group2.add(buttonNumber2);
    	group2.add(buttonNumber3);
    	group2.add(buttonNumber4);
    	group2.add(buttonNumber5);
    	group2.add(buttonNumber6);
    	group2.add(buttonNumber7);
    	group2.add(buttonNumber8);
    	group2.add(buttonNumber9);
    	group2.add(buttonNumber10);

    	p2.add(new JLabel("Number of Dice"));

    	p2.add(buttonNumber1);
    	p2.add(buttonNumber2);
    	p2.add(buttonNumber3);
    	p2.add(buttonNumber4);
    	p2.add(buttonNumber5);
    	p2.add(buttonNumber6);
    	p2.add(buttonNumber7);
    	p2.add(buttonNumber8);
    	p2.add(buttonNumber9);
    	p2.add(buttonNumber10);

  		JPanel p3 = new JPanel();
  		JButton roll = new JButton("Roll Dice");
		JButton clear = new JButton("Clear");
  		p3.add(results);
  		p3.add(roll);
  		p3.add(clear);

  		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);

		d2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				nSides = 2;
			}
		});
		d4.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 4;
					}
		});
		d6.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 6;
					}
		});
		d8.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 8;
					}
		});
		d10.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 10;
					}
		});
		d12.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 12;
					}
		});
		d20.addActionListener(new ActionListener(){
					@Override
					public void actionPerformed(ActionEvent e){
						nSides = 20;
					}
		});

  		d100.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				nSides = 100;
			}
		});
		buttonNumber1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 1;
			}
		});
		buttonNumber2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 2;
			}
		});
		buttonNumber3.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 3;
			}
		});
		buttonNumber4.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 4;
			}
		});
		buttonNumber5.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 5;
			}
		});
		buttonNumber6.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 6;
			}
		});
		buttonNumber7.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 7;
			}
		});
		buttonNumber8.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 8;
			}
		});
		buttonNumber9.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 9;
			}
		});
		buttonNumber10.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				number = 10;
			}
		});

		roll.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				int sum = 0;
				int roll = 0;
				Random r = new Random();

				for (int i = 0; i < number; i++) {
					if(roll == 0){
						roll = r.nextInt(nSides) + 1;
						results.append("Roll is " + roll);
						sum = sum + roll;
					}
					else{
						roll = r.nextInt(nSides) + 1;
						results.append("\nRoll is " + roll);
						sum = sum + roll;
					}
				}
				results.append("\nResult is " + sum + "\n\n");
			}
		});
		clear.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e){
				results.setText("");
			}
		});
  	}

 }
