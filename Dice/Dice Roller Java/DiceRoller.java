import java.util.*;
import javax.swing.*;

public class DiceRoller {
	public static void main(String[] args) {

		DiceLayout frame = new DiceLayout();
		frame.setSize(625, 300);
		frame.setTitle("Dice Roller");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setVisible(true);
	}
}