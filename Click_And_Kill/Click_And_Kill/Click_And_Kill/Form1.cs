﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Click_And_Kill
{

    
    public partial class Form1 : Form
    {
        Random r = new Random();
        bool pause = false;       
        int[] x;
        int[] y;
        int score = 0;     
       
        
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Visible = false;
            pictureBox1.Enabled = false;
            pictureBox2.Visible = false;
            pictureBox2.Enabled = false;
            pictureBox3.Enabled = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox4.Enabled = false;
            pictureBox5.Visible = false;
            pictureBox5.Enabled = false;
            gameover.Visible = false;
            highscore.Visible = false;
            amount.Text = Convert.ToString(score);
            gameover.ForeColor = System.Drawing.Color.Red;
            x = new int[10];
            y = new int[10];
            
            
        }        

        private void timer1_Tick(object sender, EventArgs e)
        {               
            x[1] += 10;
            pictureBox1.Location = new Point(x[1], y[1]);
            if (pictureBox1.Location.X >= 900)
            {
                x[1] = 12;
                progressBar1.Value += 1;
                            
            }
            if (progressBar1.Value >= progressBar1.Maximum)
            {
                gameover.Visible = true;
                highscore.Visible = true;
                highscore.Text = String.Format("HIGH SCORE: {0}", score);   
                timer1.Stop();
                timer1.Dispose();
                score = 0;
                pictureBox1.Enabled = false;
                pictureBox1.Visible = false;
                pictureBox2.Enabled = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox3.Enabled = false;
                pictureBox4.Visible = false;
                pictureBox4.Enabled = false;
                pictureBox5.Visible = false;
                pictureBox5.Enabled = false;
                
                progressBar1.Value = 0;
                        
                
            }
            if (score < 5)
            {
                timer1.Interval = 250;           
            }
            if(score > 5)
            {               
                pictureBox2.Location = new Point(x[2], y[2]);
                timer1.Interval = 175;
                pictureBox2.Enabled = true;
                pictureBox2.Visible = true;
                x[2] += 10;
                
                if (pictureBox2.Location.X >= 900)
                {
                    x[2] = 3;
                    progressBar1.Value += 1;

                }
            }
            if(score > 15)
            {
                pictureBox3.Location = new Point(x[3], y[3]);
                timer1.Interval = 125;
                x[3] += 10;                
                pictureBox3.Enabled = true;
                pictureBox3.Visible = true;
                
                if (pictureBox3.Location.X >= 900)
                {
                    x[3] = 3;
                    progressBar1.Value += 1;
                }
            }
            if (score > 30)
            {
                pictureBox4.Location = new Point(x[4], y[4]);
                timer1.Interval = 100;
                x[4] += 10;
                pictureBox4.Enabled = true;
                pictureBox4.Visible = true;
                
                if (pictureBox4.Location.X >= 900)
                {
                    x[4] = 3;
                    progressBar1.Value += 1;
                }
            }
            if (score > 40)
            {
                pictureBox5.Location = new Point(x[5], y[5]);
                timer1.Interval = 75;
                x[5] += 10;
                pictureBox5.Enabled = true;
                pictureBox5.Visible = true;                
                if (pictureBox5.Location.X >= 900)
                {
                    x[5] = 3;
                    progressBar1.Value += 1;
                }
            }
            
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Interval = 250;
            for (int i = 0; i < 10; i++)
            {
                x[i] = 3;
            }
            y[1] = 123;
            y[2] = 22;
            y[3] = 235;
            y[4] = 69;
            y[5] = 179;
            pictureBox1.Enabled = true;
            pictureBox1.Visible = true;
            pictureBox2.Enabled = false;
            pictureBox2.Visible = false;
            pictureBox3.Enabled = false;
            pictureBox3.Visible = false;
            pictureBox4.Enabled = false;
            pictureBox4.Visible = false;
            pictureBox5.Enabled = false;
            pictureBox5.Visible = false;
            pictureBox1.Location = new Point(3, 123);
            gameover.Visible = false;
            highscore.Visible = false;
            progressBar1.Value = 0;
            score = 0;
            amount.Text = Convert.ToString(score);
            timer1.Start();          
            
        }

        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pause == false)
            {
                timer1.Stop();                
                pause = true;
                pauseToolStripMenuItem.Text = "Continue";
            }
            else
            {
                timer1.Start();                
                pause = false;
                pauseToolStripMenuItem.Text = "Pause";
            }
        }      

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            timer1.Dispose();            
            this.Close();
        }       

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            pictureBox1.Enabled = false;
            pictureBox1.Location = new Point(3, 123);
            score++;
            amount.Text = Convert.ToString(score);
            pictureBox1.Enabled = true;
            x[1] = 3;
            y[1] = 123;  
         }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            pictureBox2.Enabled = false;
            pictureBox2.Location = new Point(3, 22);
            score++;
            amount.Text = Convert.ToString(score);
            pictureBox2.Enabled = true;
            x[2] = 3;
            y[2] = 22;  
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            pictureBox3.Enabled = false;
            pictureBox3.Location = new Point(3, 235);
            score++;
            amount.Text = Convert.ToString(score);
            pictureBox3.Enabled = true;
            x[3] = 3;
            y[3] = 235;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            pictureBox4.Enabled = false;
            pictureBox4.Location = new Point(3, 69);
            score++;
            amount.Text = Convert.ToString(score);
            pictureBox4.Enabled = true;
            x[4] = 3;
            y[4] = 69;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            pictureBox5.Enabled = false;
            pictureBox5.Location = new Point(3, 179);
            score++;
            amount.Text = Convert.ToString(score);
            pictureBox5.Enabled = true;
            x[5] = 3;
            y[5] = 179;
        }       

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.Show();
        }


        

        
        
        
    }
}
