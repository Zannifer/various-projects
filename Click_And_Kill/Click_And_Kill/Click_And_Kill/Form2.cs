﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Click_And_Kill
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            f2label1.Text = String.Format("To play game, press New Game in main window.");
            f2label1.Text += String.Format("\nPause will pause the game, Continue will resume.");
            f2label1.Text += String.Format("\nExit will close the game entirely.");
            f2label1.Text += String.Format("\n\n\nCreated by Cody Schalue\nCS 3110");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
